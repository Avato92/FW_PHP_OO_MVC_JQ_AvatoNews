<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>AvatoNews</title>
    <!-- BOOTSTRAP CORE STYLE CSS -->
    <link href="<?php echo CSS_PATH ?>animate.min.css" rel="stylesheet">
    <link href="<?php echo CSS_PATH ?>bootstrap.css" rel="stylesheet" />
    <!-- FONTAWESOME STYLE CSS -->
    <link href="<?php echo CSS_PATH ?>font-awesome.min.css" rel="stylesheet" />
    <!-- CUSTOM STYLE CSS -->
    <link href="<?php echo CSS_PATH ?>style.css" rel="stylesheet" />
    <!-- GOOGLE FONT -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  	<script src="//code.jquery.com/jquery-1.10.2.js"></script>
  	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
    <script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
    <!--UTILS JS-->
    <script type="text/javascript" src="<?php echo JS_PATH ?>main.js"></script>
    <script type="text/javascript" src="<?php echo JS_PATH ?>cookies.js"></script>
    <script type="text/javascript" src="<?php echo JS_PATH_LOGIN ?>init.js"></script>
</head>
<body>