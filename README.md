Proyecto de la 3º evaluación de Programación de aplicaciones web con un Framework de PHP OO con JavaScript y con JQuery, por Alejandro Vañó Tomás.
Avato News, se trata de una web de noticias.

Hecho:
-Formulario y vista de resultados
-Validación JS y PHP
-Insert Into en base de datos
-Dependent dropdowns
-Dropzone
-List, con scroll, de las subcategorias y de las categorias especificas, busqueda de todas las noticias o solo de la categoria especificada.
-Paginador
-Autocomplete
-Router, autoload, pretty url
-Módulo de contact
-Módulo API google maps
-Details
-Login de FB, TW y G+
-Login manual
-Envio de mensaje, cuando se da de alta y cuando desea cambiar la contraseña
-Profile
-Confirmación del email

Mejoras:
-El módulo de google maps, busca por paises, autocompleta con todas las ciudades de ese país y cuanndo se selecciona una ciudad, busca en base de datos todas las noticias comprendidas entre las coordenadas topes de esa ciudad


Pendiente:


Mejoras pendientes:
- Extraer la información de los txt, subidos por el dropzone
- Aumentar las visitas de las noticias, cuando abren los detalles de la noticia
- Categorias ordenadas por visitas