<?php

class controller_international {
    function __construct() {
        $_SESSION['module'] = "international";
    }

    function map() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/international/view/', 'map.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function list_news() {
    	$coordenadas = $_POST['key'];
        $json = array();

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/international/model/model/';
        $json = loadModel($path_model, "international_model", "search_news", $coordenadas);
        echo json_encode($json);
        exit();
    }

    function details_new() {
        $_SESSION['id'] = $_GET['aux'];
      
         require_once(VIEW_PATH_INC . "header.php");
         require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/international/view/', 'details.html');

         require_once(VIEW_PATH_INC . "footer.html");
    }

    function search_details() {
        $id = $_SESSION['id'];
        $json = array();

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/international/model/model/';
        $json = loadModel($path_model, "international_model", "search_details", $id);
        echo json_encode($json);
        exit();
    }
    
}