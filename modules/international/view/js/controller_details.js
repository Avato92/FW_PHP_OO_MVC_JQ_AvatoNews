 $(document).ready(function(){
   $.ajax({
              type: "POST",
              url: "../../international/search_details/",
              success: function(data, status){
              
              var info = JSON.parse(data);
              print_new(info);
              }
    });
 });

 function print_new(info) {
    var content = document.getElementById("content");
    var div_product = document.createElement("div");
    var parrafo = document.createElement("p");

    var author = document.createElement("div");
    author.innerHTML = "Autor: ";
    author.innerHTML += info[0].author;

    var event_date = document.createElement("div");
    event_date.innerHTML = "Fecha del suceso: ";
    event_date.innerHTML += info[0].event_date;

    var publication_date = document.createElement("div");
    publication_date.innerHTML = "Fecha de publicación: ";
    publication_date.innerHTML += info[0].publication_date;


    var headline = document.createElement("div");
    headline.innerHTML = "Titular: ";
    headline.innerHTML += info[0].headline;

    var category = document.createElement("div");
    category.innerHTML = "Categoria: ";
    category.innerHTML += info[0].category;

    var path = document.createElement("div");
    path.innerHTML = "Ruta de la noticia: ";
    path.innerHTML += info[0].path;

    div_product.appendChild(parrafo);
    parrafo.appendChild(author);
    parrafo.appendChild(headline);
    parrafo.appendChild(event_date);
    parrafo.appendChild(publication_date);
    parrafo.appendChild(category);
    parrafo.appendChild(path);
    content.appendChild(div_product);
}