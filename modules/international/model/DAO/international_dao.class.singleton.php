<?php

class international_DAO {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function search_news_DAO($db, $coordenadas){
        $sql = "SELECT * FROM news WHERE longitud BETWEEN '" . $coordenadas['bounds'] . "' AND '" . $coordenadas['bounds2'] . "' AND latitud BETWEEN '" . $coordenadas['bounds3'] . "' AND '" . $coordenadas['bounds4'] . "'";
        return $db->listar($db,$sql);
    }

    public function search_details_DAO($db, $id){
        $sql = "SELECT * FROM news WHERE id_new = '$id'";

        return $db->listar($db,$sql);
    }
}