<?php

class international_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = international_bll::getInstance();
    }
    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function search_news($coordenadas){
    	return $this->bll->search_news_BLL($coordenadas);
    }
    public function search_details($id){
        return $this->bll->search_details_BLL($id);
    }

}