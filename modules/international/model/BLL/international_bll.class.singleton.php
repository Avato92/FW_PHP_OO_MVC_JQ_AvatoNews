<?php

class international_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = international_DAO::getInstance();
        $this->db = Db::getInstance();
    }
    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function search_news_BLL($coordenadas){
      return $this->dao->search_news_DAO($this->db, $coordenadas);
    }
    public function search_details_BLL($id){
        return $this->dao->search_details_DAO($this->db, $id);
    }
}