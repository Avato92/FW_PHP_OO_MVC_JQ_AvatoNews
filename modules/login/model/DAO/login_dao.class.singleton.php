<?php

class login_DAO {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function search_social_network_user_DAO($db, $user){
    	$id = $user['id'];
    	$sql = "SELECT COUNT(*) FROM users WHERE id = '$id'";

    	return $db->listar($db, $sql);
    }
    public function insert_user_DAO($db, $user){
    	$name = $user['name'];
    	$email = $user['email'];
    	$id = $user['id'];
    	$photo = $user['photo'];

    	$sql = "INSERT INTO users (id, name_user, email, photo, sign_in, confirmed, type_user) VALUES('$id', '$name', '$email', '$photo', now(), true, 'reader')";

    	return $db->ejecutar($sql);
    }
    public function select_user_DAO($db, $id){
    	$sql = "SELECT * FROM users WHERE id = '$id'";

    	return $db->listar($db, $sql);
    }
    public function search_user_DAO($db, $user){
        $sql = "SELECT COUNT(*) FROM users WHERE name_user = '$user' AND id IS NULL";

        return $db->listar($db, $sql);
    }
    public function insert_user_manual_DAO($db, $user){
        $name = $user['name'];
        $email = $user['email'];
        $password = $user['password'];
        $photo = $user['photo'];
        $token = $user['token'];

        $sql = "INSERT INTO users (name_user, email, password_user, photo, sign_in, token, type_user, confirmed) VALUES ('$name', '$email', '$password', '$photo', now(), '$token', 'reader', false)";

        return $db->ejecutar($sql);
    }
    public function search_email_DAO($db, $email){
        $sql = "SELECT COUNT(*) FROM users WHERE email = '$email' AND id IS NULL";

        return $db->listar($db, $sql);
    }
    public function verify_password_DAO($db, $user){
        $sql = "SELECT password_user FROM users WHERE name_user = '$user'";

        return $db->listar($db, $sql);
    }
    public function select_user_manual_DAO($db, $user){
        $sql = "SELECT * FROM users WHERE name_user = '$user'";

        return $db->listar($db, $sql);
    }
    public function search_email_recover_DAO($db, $email){
        $sql = "SELECT email, confirmed FROM users WHERE email = '$email' AND id IS NULL";

        return $db->listar($db, $sql);
    }
    public function update_token_DAO($db, $data){
        $email = $data['email'];
        $token = $data['token'];

        $sql = "UPDATE users SET token = '$token' WHERE email = '$email' AND id IS NULL";

        return $db->ejecutar($sql);
    }
    public function search_token_DAO($db, $token){
        $sql = "SELECT COUNT(*) FROM users WHERE token = '$token'";

        return $db->listar($db, $sql);
    }
    public function update_password_DAO($db, $data){
        $token = $data['token'];
        $password = $data['password'];

        $sql = "UPDATE users SET password_user = '$password' WHERE token = '$token'";

        return $db->ejecutar($sql);
    }
    public function confirm_email_DAO($db, $token){
        $sql = "UPDATE users SET confirmed = true WHERE token = '$token'";

        return $db->ejecutar($sql);
    }
    public function update_profile_DAO($db, $data){
        $user = $data['user'];
        $name = $data['name'];
        $surname = $data['surname'];

        if (($name == "") AND ($surname == "")){

            return 0;
        }else if($name == ""){
            $sql = "UPDATE users SET surname = '$surname' WHERE name_user = '$user'";

            return $db->ejecutar($sql);
        }else if($surname == ""){
            $sql = "UPDATE users SET original_name = '$name' WHERE name_user = '$user'";

            return $db->ejecutar($sql);
        }else{
            $sql = "UPDATE users SET original_name = '$name', surname = '$surname' WHERE name_user = '$user'";

            return $db->ejecutar($sql);
        }
    }
}