<?php

class login_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = login_DAO::getInstance();
        $this->db = Db::getInstance();
    }
    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function search_social_network_user_BLL($user){
      return $this->dao->search_social_network_user_DAO($this->db, $user);
    }
    public function insert_user_BLL($user){
        return $this->dao->insert_user_DAO($this->db, $user);
    }
    public function select_user_BLL($id){
        return $this->dao->select_user_DAO($this->db, $id);
    }
    public function search_user_BLL($user){
        return $this->dao->search_user_DAO($this->db, $user);
    }
    public function insert_user_manual_BLL($user){
        return $this->dao->insert_user_manual_DAO($this->db, $user);
    }
    public function search_email_BLL($email){
        return $this->dao->search_email_DAO($this->db, $email);
    }
    public function verify_password_BLL($user){
        return $this->dao->verify_password_DAO($this->db, $user);
    }
    public function select_user_manual_BLL($user){
        return $this->dao->select_user_manual_DAO($this->db, $user);
    }
    public function search_email_recover_BLL($email){
        return $this->dao->search_email_recover_DAO($this->db, $email);
    }
    public function update_token_BLL($data){
        return $this->dao->update_token_DAO($this->db, $data);
    }
    public function search_token_BLL($token){
        return $this->dao->search_token_DAO($this->db, $token);
    }
    public function update_password_BLL($data){
        return $this->dao->update_password_DAO($this->db, $data);
    }
    public function confirm_email_BLL($token){
        return $this->dao->confirm_email_DAO($this->db, $token);
    }
    public function update_profile_BLL($data){
        return $this->dao->update_profile_DAO($this->db, $data);
    }
}