<?php

class controller_login {
    function __construct() {

        require_once(UTILS_PATH_LOGIN . "functions.inc.php");
        include (LIBS . 'password_compat-master/lib/password.php');
        $_SESSION['module'] = "login";
        require_once(UTILS_PATH . "mail.inc.php");
    }

    function signin() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/login/view/', 'signin.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function find_social_network(){

    	$count = array();

      	$path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/login/model/model/';
      	$count = loadModel($path_model, "login_model", "search_social_network_user", $_POST['user']);
    
      	if($count){
      	     $number["exist"] = $count[0]["COUNT(*)"];
             if($number['exist'] == 0){
        		$insert = loadModel($path_model, "login_model", "insert_user", $_POST['user']);
        		if($insert){
        			$user = loadModel($path_model, "login_model", "select_user", $_POST['user']['id']);
        			echo json_encode($user);
        			exit();
        		}else{
        			echo json_encode("No insertado");
        			exit();
        		}
        	 }else{
        		$user = array();
        		$user = loadModel($path_model, "login_model", "select_user", $_POST['user']['id']);
        		echo json_encode($user);
        		exit();
        	}
      	}else{
        	$json = "error";
        	echo json_encode($json);
        	exit;
      	}

    }
        function register() {
            require_once(VIEW_PATH_INC . "header.php");
            require_once(VIEW_PATH_INC . "menu.php");

            loadView('modules/login/view/', 'register.html');

            require_once(VIEW_PATH_INC . "footer.html");
    }

    function save_user(){
        $count = array();

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/login/model/model/';
        $count = loadModel($path_model, "login_model", "search_user", $_POST['user']['name']);
    
        if($count){
             $number["exist"] = $count[0]["COUNT(*)"];
             if($number['exist'] == 0){
                $count_email = loadModel($path_model, "login_model", "search_email", $_POST['user']['email']);
                if($count_email){
                    $number["exist_email"] = $count_email[0]["COUNT(*)"];
                    if($number['exist_email'] == 0){
                        $photo = get_gravatar($_POST['user']['email'], $s = 400, $d = 'identicon', $r = 'g', $img = false, $atts = array());
                        $token = "Ver" . md5(uniqid(rand(), true));

                        $user = array(
                            'name' => $_POST['user']['name'],
                            'email' => $_POST['user']['email'],
                            'password' => password_hash($_POST['user']['password'], PASSWORD_BCRYPT),
                            'photo' => $photo,
                            'token' => $token
                        );
                        $insert = loadModel($path_model, "login_model", "insert_user_manual", $user);
                        if($insert == true){
                                $data = array(
                                    'token' => $token,
                                    'email' => $_POST['user']['email']
                                );
                                sendtoken($data, "alta");
                    
                                $jsondata["success"] = true;
                                $jsondata["user"] = $user;
                                echo json_encode($jsondata);
                                exit();
                        }else{
                            echo json_encode("3");
                            exit();
                        }
                    }else{
                        echo json_encode("2");
                        exit();
                    }
                }
               
            }else{
                echo json_encode("1");
                exit();
            }
        }
    }

    function sign_in(){
        $user = $_POST['user']['name'];
        $password = $_POST['user']['password'];

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/login/model/model/';
        $pass = loadModel($path_model, "login_model", "verify_password", $user);

        if($pass != "error"){
            $confirm = password_verify($password, $pass[0]['password_user']);
            if($confirm ==  true){
                $person['success'] = true;
                $person['user'] = loadModel($path_model, "login_model", "select_user_manual", $user);
                echo json_encode($person);
                exit();
            }else{
                echo json_encode(2);
                exit();
            }
        }else{
            echo json_encode(1);
            exit();
        }
    }

    function recover_password(){
            require_once(VIEW_PATH_INC . "header.php");
            require_once(VIEW_PATH_INC . "menu.php");

            loadView('modules/login/view/', 'recover.html');

            require_once(VIEW_PATH_INC . "footer.html");
    }

    function search_email(){
        $email = $_POST['email'];

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/login/model/model/';
        $mail = loadModel($path_model, "login_model", "search_email_recover", $email);

        if($mail == "error"){
            echo json_encode(1);
            exit();
        }else{
            if($mail[0]['confirmed'] == false){
                echo json_encode(2);
                exit();
            }else{

                $token = "Cha" . md5(uniqid(rand(), true));
                $arrArgument = array(
                            'token' => $token,
                            'email' => $email
                        );
                $update = loadModel($path_model, "login_model", "update_token", $arrArgument);
                sendtoken($arrArgument, "modificacion");
                echo json_encode(0);
                exit();
            }
        }
    }
    function change_password(){
        if (substr($_GET['aux'], 0, 3) == "Cha") {
            $_SESSION['cha'] = $_GET['aux'];
            
            require_once(VIEW_PATH_INC . "header.php");
            require_once(VIEW_PATH_INC . "menu.php");

            loadView('modules/login/view/', 'change_pass.html');

            require_once(VIEW_PATH_INC . "footer.html");
        }
    }

    function update_password(){
        $token_user = $_SESSION['cha'];
        $password = password_hash($_POST['new_password'], PASSWORD_BCRYPT);

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/login/model/model/';

        $token = loadModel($path_model, "login_model", "search_token", $token_user);

        if($token){
             $user["exist"] = $token[0]["COUNT(*)"];
             if ($user["exist"] == 1){
                $data = array(
                    'token' => $token_user,
                    'password' => $password
                );
                $confirm = loadModel($path_model, "login_model", "update_password", $data);
                
                if ($confirm == true){
                    echo json_encode(0);
                    exit();
                }else{
                    echo json_encode(1);
                    exit();
                }
             }else{
                echo json_encode(2);
                exit();
             }

        }
    }

    function profile(){
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/login/view/', 'profile.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function confirm_email(){
        if (substr($_GET['aux'], 0, 3) == "Ver") {

            $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/login/model/model/';

            try {
                $token = loadModel($path_model, "login_model", "search_token", $_GET['aux']);
            } catch (Exception $e) {
                $token = false;
            }

            if($token){
                $user["exist"] = $token[0]["COUNT(*)"];
                if ($user["exist"] == 1){
                    $confirm = loadModel($path_model, "login_model", "confirm_email", $_GET['aux']);
                }
                if ($confirm){
                    require_once(VIEW_PATH_INC . "header.php");
                    require_once(VIEW_PATH_INC . "menu.php");

                    loadView('modules/main/view/', 'category.html');

                    require_once(VIEW_PATH_INC . "footer.html");
                }
            }
        }
    }
    function update_profile(){

        $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/login/model/model/';

        $confirm = loadModel($path_model, "login_model", "update_profile", $_POST['profile']);

        echo json_encode($confirm);
        exit();
    }
}