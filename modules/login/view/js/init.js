$(document).ready(function () {
	var user = Tools.readCookie("user");
    // console.log('user: ' + user);
    if (user) {
        user = user.split("|");
        
        $("#LogProf").html("<li><a id='profile' href='"+ amigable('?module=login&function=profile') + "'>" + user[0] + "</a></li>");
        $("#photo").html("<li><img id='menuImg' width='50' height='50' class='icon rounded' src='" + user[1] + "'/></li>");
        $("#LogProf").after("<li><a id='logout' href='#' >SALIR</a></li>");
        if ( (user[4] === "journalist") || (user[4] === "reader")  ) {
            // $("#LogProf").before("<li><a href=" + amigable('?module=ofertas') + ">Mis ofertas</a></li>")
        } else if (user[4] === "admin") {
            // $("#LogProf").before("<li><a href=" + amigable('?module=admin') + ">Administrar</a></li>")
        }
        $("head").append("<script src='http://127.0.0.1/FW_PHP_OO_JQ_AvatoNews/modules/login/view/js/logout.js'></script>");
        $("#login").hide();
    }

    var url = window.location.href;
    // console.log('url: ' + url);
    //https://php-mvc-oo-yomogan.c9users.io/3_Framework/7_proj_final_login/JoinElderly/main/begin/reg/
    
    //en controller_user
    //amigable('?module=main&function=begin&param=reg', true);
    //amigable("?module=main&fn=begin&param=503");
    //amigable('?module=main&function=begin&param=rest', true);
    //amigable('?module=user&function=profile&param=done', true);
    //amigable('?module=user&function=profile&param=503', true);
    
    //en mail.inc.php
    //$ruta = '<a href=' . amigable("?module=user&function=activar&aux=" . $arr['token'], true) . '>aqu&iacute;</a>';
    
    // url = url.split("/");
    // console.log('url5: ' + url[5]); //JoinElderly
    // console.log('url6: ' + url[6]); //main
    // console.log('url7: ' + url[7]); //begin
    // console.log('url8: ' + url[8]); //reg
    
    if (url[7] === "activar" && url[8].substring(0, 3) == "Ver"){
        $("#alertbanner").html("<a href='#alertbanner' class='alertbanner'>Su email ha sido verificado, disfrute de nuestros servicios</div>");
    }else if(url[8]==="503"){
         $("#alertbanner").html("<a href='#alertbanner' class='alertbanner alertbannerErr'>Hay un problema en la base de datos, inténtelo más tarde</div>");
    }else if (url[7] === "begin") {
        if (url[8] === "reg"){
            $("#alertbanner").html("<a href='#alertbanner' class='alertbanner'>Se le ha enviado un email para verificar su cuenta</div>");
        }else if (url[8] === "rest"){
            $("#alertbanner").html("<a href='#alertbanner' class='alertbanner'>Se ha cambiado satisfactoriamente su contraseña</div>");
        }
    } else if (url[7] === "profile"){
        if (url[8] === "done")
            $("#alertbanner").html("<a href='#alertbanner' class='alertbanner'>Usuario correctamente actualizado</div>");
    }
});
