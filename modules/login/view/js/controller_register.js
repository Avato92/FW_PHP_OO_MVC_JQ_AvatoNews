$(document).ready(function () {
	$( "#register" ).click(function() {
		var validate = validate_user();
			if (validate == false){
				alert("No valido");
			}
	});
});

function validate_user() {
    var result = true;
    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var nomreg = /^\D{3,30}$/;
    var usuario = $("#name").val();
    var email = $("#email").val();
    var password = $("#password").val();
    var password2 = $("#password2").val();

    $(".error").remove();
    if ($("#name").val() === "" || !nomreg.test($("#name").val())) {
        $("#error_name").focus().after("<span class='error'>Usuario no válido</span>");
        result = false;
    } else if ($("#name").val().length < 3) {
        $("#error_name").focus().after("<span class='error'>Mínimo 3 carácteres para el usuario</span>");
        result = false;
    }if (!emailreg.test($("#email").val()) || $("#email").val() === "") {
        $("#error_email").focus().after("<span class='error'>Ingrese un email correcto</span>");
        result = false;
    }if ($("#password").val() === "") {
        $("#error_password").focus().after("<span class='error'>Ingrese su contraseña</span>");
        result = false;
    } else if ($("#password").val().length < 6) {
        $("#error_password").focus().after("<span class='error'>Mínimo 6 carácteres para la contraseña</span>");
        result = false;
    }if ($("#password2").val() !== $("#password2").val()) {
        $("#error_password2").focus().after("<span class='error'>Debe coincidir con la contraseña</span>");
        result = false;
    }

    if (result) {
        var user_data = {"name": usuario, "email": email,
            "password": password};
        $.ajax({
    						  type: "POST",
    						  url: amigable("?module=login&function=save_user"),
    						  data: {user: user_data},
    						  success: function(data, status){
                               
                                var json_user = JSON.parse(data);
                                 console.log(json_user);

                                if (json_user == 1){
                                    $("#error_name").focus().after("<span class='error'>Nombre ya en uso</span>");
                                }else if (json_user == 2){
                                    $("#error_email").focus().after("<span class='error'>Email en uso</span>");
                                }else if (json_user == 3){
                                    alert("Problemas de conexión, intentelo más tarde")
                                    window.location.href = amigable("?module=main&function=begin");
                                }else{
                                    if(json_user.success){
                                        Tools.createCookie("user", json_user.user.name + "|" + json_user.user.photo + "|" + json_user.user.email + "|" + "null" + "|" + "reader", 1);
                                        window.location.href = amigable("?module=main&function=begin");
                                    }
                                }
    						  }
    						});
    }
}