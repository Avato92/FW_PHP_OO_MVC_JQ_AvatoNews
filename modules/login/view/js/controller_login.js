$(document).ready(function () {
	$( "#create_user" ).click(function() {
		window.location.href = amigable("?module=login&function=register");
	});
	$("#sign_in").click(function(){
		var name = $("#name").val();
		var password = $("#password").val();
		var user = {"name" : name, "password" : password};

		$.ajax({
    		type: "POST",
    		url: amigable("?module=login&function=sign_in"),
    		data: {user: user},
    		success: function(data, status){

    			var user = JSON.parse(data);

    			if(user == 1){
    				$("#name").focus().after("<span class='error'>Nombre incorrecto</span>");
    			}else if(user == 2){
    				$("#password").focus().after("<span class='error'>Contraseña erronea</span>");
    			}else{
    				if(user.success){
    					Tools.createCookie("user", user.user[0].name_user + "|" + user.user[0].photo + "|" + user.user[0].email + "|" + "null" + "|" + user.user[0].type_user, 1);
                        window.location.href = amigable("?module=main&function=begin");
    				}
    			}
    		}
    	});
	});
	$("#recover_password").click(function(){
		window.location.href = amigable("?module=login&function=recover_password");
	});
});