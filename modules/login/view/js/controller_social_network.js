
    	
$(document).ready(function(){
	 var config = {
    apiKey: "AIzaSyAhVBoYt0Ufv9wWY6ifWXT1KweeTj8JHEg",
    authDomain: "eastern-kit-192121.firebaseapp.com",
    databaseURL: "https://eastern-kit-192121.firebaseio.com",
    projectId: "eastern-kit-192121",
    storageBucket: "eastern-kit-192121.appspot.com",
    messagingSenderId: "725158293198"
  };
  firebase.initializeApp(config);

        

		$( "#fb_login" ).click(function() {
			var provider = new firebase.auth.FacebookAuthProvider();

        	var authService = firebase.auth();

  		 		authService.signInWithPopup(provider)
                .then(function(result) {

                  var person = {name:result.user.displayName, email:result.user.email, photo:result.user.photoURL, id:result.user.uid};

                $.ajax({
    						  type: "POST",
    						  url: "../../login/find_social_network/",
    						  data: {user: person},
    						  success: function(data, status){
    							 var user = JSON.parse(data);

                    if (!user.error) {
                      Tools.createCookie("user", user[0]['name_user'] + "|" + user[0]['photo'] + "|" + user[0]['email'] + "|" + user[0]['id'] + "|" + user[0]['type_user'], 1);
                      window.location.href = amigable("?module=main&function=begin");
                }
  						}
					});
                })
                .catch(function(error) {
                console.log('Detectado un error:', error);
        });
	});
		$( "#tw_login" ).click(function() {
  		 	var provider = new firebase.auth.TwitterAuthProvider();
      		var authService = firebase.auth();
  
          	authService.signInWithPopup(provider).then(function(result) {
          		var person = {name:result.user.displayName, email:result.user.email, photo:result.user.photoURL, id:result.user.uid};
            
            	$.ajax({
  						type: "POST",
  						url: "../../login/find_social_network/",
  						data: {user: person},
  						success: function(data, status){
  							var user = JSON.parse(data);

                if (!user.error) {
                  Tools.createCookie("user", user[0]['name_user'] + "|" + user[0]['photo'] + "|" + user[0]['email'] + "|" + user[0]['id'] + "|" + user[0]['type_user'], 1);
                  window.location.href = amigable("?module=main&function=begin");
                }
  						}
				});
          	}).catch(function(error) {
               console.log('Detectado un error:', error);
        });
    })
		$( "#g_login" ).click(function() {
  		 	 var provider = new firebase.auth.GoogleAuthProvider();
        	provider.addScope('email');
    
        	var authService = firebase.auth();
          	authService.signInWithPopup(provider).then(function(result) {
          		var person = {name:result.user.displayName, email:result.user.email, photo:result.user.photoURL, id:result.user.uid};

            	$.ajax({
  						type: "POST",
  						url: "../../login/find_social_network/",
  						data: {user: person},
  						success: function(data, status){

  							var user = JSON.parse(data);

                if (!user.error) {
                  Tools.createCookie("user", user[0]['name_user'] + "|" + user[0]['photo'] + "|" + user[0]['email'] + "|" + user[0]['id'] + "|" + user[0]['type_user'], 1);
                  window.location.href = amigable("?module=main&function=begin");
                }
  						}
				});
          	}).catch(function(error) {
        });
    })
});