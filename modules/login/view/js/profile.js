$(document).ready(function () {
	$("#progress").hide();

    $("#submitBtn_user").click(function(){
        var name = $("#inputName").val();
        var surname = $("#inputSurn").val();

        var profile_data = {"name" : name, "surname": surname, "user" : user[0]};

        $.ajax({
            type: "POST",
            url: amigable("?module=login&function=update_profile"),
            data: {profile: profile_data},
            success: function(data, status){
                console.log(data);
            
            var result = JSON.parse(data);

            if (result == true){
                alert("Datos modificados con éxito");
                window.location.href = amigable("?module=main&function=begin");
            }else{
                alert("Error, no se puede modificar, intentelo más tarde");
            }
                              }
        });
    });

	var user = Tools.readCookie("user");
    if (user) {
        user = user.split("|");

        $("#username").html(user[0]);
        $("#avatar_user").attr('src', user[1]);
        $("#inputEmail").val(user[2]);
    }
});