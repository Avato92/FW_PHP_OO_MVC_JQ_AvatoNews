$(document).ready(function () {
    $("#logout").click(function () {
        logout();
    });
});

function logout(){
    Tools.eraseCookie("user");
    Tools.eraseCookie("tw");
    window.location.href = amigable("?module=main&function=begin");
}
