$(document).ready(function(){
	$("#sent_email").click(function(){
		var email = $("#email").val();
		$.ajax({
    		type: "POST",
    		url: amigable("?module=login&function=search_email"),
    		data: {email: email},
    		success: function(data, status){
    			var result = JSON.parse(data);

                if ( result == 0){
                    window.location.href = amigable("?module=main&function=begin");
                }else if(result == 1){
                    $("#email").focus().after("<span class='error'>Email incorrecto</span>");
                }else if(result == 2){
                    $("#email").focus().after("<span class='error'>Correo no verificado</span>");
                }
    		}
    	});
	});
});