
$(document).ready(function(){
	var count = 0;
	$("#changeBtn").click(function(){
		var password = $("#password").val();
		var password2 = $("#password2").val();

		if (password === ""){
        	$("#password").focus().after("<span class='error'>Introduzca contraseña</span>");
		}else if(password.length < 6){
			$("#password").focus().after("<span class='error'>La contraseña debe tener más de 6 caracteres</span>");
		}else if (password != password2){
			$("#password").focus().after("<span class='error'>Las contraseñas no son iguales</span>");
		}else{
			$.ajax({
	    		type: "POST",
	    		url: amigable("?module=login&function=update_password"),
	    		data: {new_password: password},
	    		success: function(data, status){
	    			var result = JSON.parse(data);

	    			if (result == 0){
	    				alert("Contraseña modificada con exito");
	    				window.location.href = amigable("?module=login&function=signin");
	    			}else if(result == 1){
	    				$("#password").focus().after("<span class='error'>Error con el servidor, vuelva a intentarlo</span>");
	    				count ++;
	    			}else if(result == 2){
	    				$("#password").focus().after("<span class='error'>El token no es correcto</span>");
	    			}

	    			if(count == 3){
	    				alert("Ha sobreapasado el número de intentos, redirigiendo al home");
	    				window.location.href = amigable("?module=main&function=begin");
	    			}
	    		}
    		});
		}
	});

});