<?php

class main_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = main_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    public function obtain_number_pages() {
        return $this->bll->obtain_number_pages_BLL();
    }
    public function select_limit_news($array){
        return $this->bll->select_limit_news_BLL($array);
    }
    public function select_categories(){
        return $this->bll->select_categories_BLL();
    }
    public function select_categories_2($key){
        return $this->bll->select_categories_2_BLL($key);
    }
    public function select_subcategories(){
        return $this->bll->select_subcategories_BLL();
    }
    public function select_subcategories2($cat){
        return $this->bll->select_subcategories2_BLL($cat);
    }
    public function select_specific_categories($cat){
        return $this->bll->select_specific_categories_BLL($cat);
    }
    public function select_news($cat){
        return $this->bll->select_news_BLL($cat);
    }
    public function autocomplete($key){
        return $this->bll->autocomplete_BLL($key);
    }
}