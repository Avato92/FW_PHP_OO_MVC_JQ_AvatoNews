$(document).ready(function () {
	load_categories();
	$('#load_more').append("<div class='container'>" +
		"<button id='more' class='btn btn-primary'>Más subcategorias</button>" + 
		"<button id='less' class='btn btn-primary'>Menos</button>" +
		"<button id='back' class='btn btn-primary'>Atras</button>");
	$('#all_news').append("<div class='container'>" +
		"<button id='read_all_news' class='btn btn-primary'>Ver todas las noticias</button>");
	$('#less').hide();
	$('#back').hide();
	$('.pagination_news').hide();
	$('#more').click(function(){
		$.ajax({
		type: "POST",
		url: "../../main/list_subcategories/",
		success: function(data, status){
			var subcategories = JSON.parse(data);
			var num = subcategories.length;

			$('#list_subcategories').append("<div class='container'>");
			for(var i = 0; i < num; i++){
				$('#list_subcategories').append("<div class='container'>" + 
					"<button id='" + subcategories[i].cod + "' name='" + subcategories[i].cod_cat + "' class='btn btn-primary' onclick='search_news2(this)'>" + subcategories[i].name + "</button>");
			}
			$('#list_categories').html('');
			$('#more').hide();
			$('#less').show();
			$('#back').hide();
			$('#read_all_news').hide();
			}
		});

	});
	$('#less').click(function(){
		load_categories();
		$('#list_subcategories').html('');
			$('#more').show();
			$('#less').hide();
			$('#back').hide();
			$('#read_all_news').show();
	});
	$('#back').click(function(){
		$('#list_subcategories').html('');
		$('#list_specific_categories').html('');
		$('#list_news').html('');
		load_categories();
		$('#back').hide();
		$('#more').show();
		$('#read_all_news').show();
		$('.pagination_news').hide();
		reset();
	});
	$('#read_all_news').click(function(){
		list_first_time();
	});
	bootpag();
	$('#search').click(function(){
		var key = document.getElementById('autocomplete').value;
		$.ajax({
			type: "POST",
			url: "../../main/list_categories/",
			data: {key: key},
			success: function(data, status){
			var categories = JSON.parse(data);
			var num = categories.length;
				if(num > 0){
					$('#list_categories').html('');
					var categories = JSON.parse(data);



					for(var i = 0; i < num; i++){
						$('#list_categories').append("<button id='" + categories[i].cod + "' value='" + categories[i].cod + "' class='btn btn-primary' onclick='search_news(this)'>" + categories[i].name + "</button>");
					}
				}
			}
		});
	});
});

$(document).on('input', '#autocomplete',  function() {
	var key = document.getElementById('autocomplete').value;

	 $.ajax({
		type: "POST",
		url: "../../main/autocomplete/",
		data: {key: key},
		success: function(data, status){
		  	var categories = JSON.parse(data);
		  	var num = categories.length;
        
        	var suggestions = new Array();
        	for (var i = 0; i < num; i++) {
            	suggestions.push(categories[i].name);
        	}
        
        $("#autocomplete").autocomplete({
            source: suggestions,
            minLength: 1,
            select: function (event, ui) {
            	alert("Hola");
            }
        });
			}
		});
});

function search_news(comp){
  let id = comp.id;

  $.ajax({
		type: "POST",
		url: "../../main/list_subcategories/",
		data: {cat: id},
		success: function(data, status){
			var subcategories = JSON.parse(data);
			var num = subcategories.length;

			$('#list_subcategories').append("<div class='container'>");
			for(var i = 0; i < num; i++){
				$('#list_subcategories').append("<div class='container'>" + 
					"<button id='" + subcategories[i].cod + "' name='" + subcategories[i].cod_cat + "' class='btn btn-primary' onclick='search_news2(this)'>" + subcategories[i].name + "</button>");
			}
			$('#list_categories').html('');
			$('#more').hide();
			$('#back').show();
			$('#read_all_news').hide();
			}
		});

}

function search_news2(comp){
  let id = comp.id;
  let name = comp.name

  var cat = {cat: name, sub: id};

    $.ajax({
		type: "POST",
		url: "../../main/list_specific_categories/",
		data: {cat: cat},
		success: function(data, status){
			var specific_categories = JSON.parse(data);
			var num = specific_categories.length;

			$('#list_specific_categories').append("<div class='container'>");
			for(var i = 0; i < num; i++){
				$('#list_specific_categories').append("<div class='container'>" + 
					"<button id='" + specific_categories[i].cod_cat + "' value='" + specific_categories[i].cod_sub + "' name='" + specific_categories[i].cod + "' class='btn btn-primary' onclick='search_news3(this)'>" + specific_categories[i].concrete_name + "</button>");
			}
			$('#list_categories').html('');
			$('#list_subcategories').html('');
			$('#less').hide();
			$('#more').hide();
			$('#back').show();
			$('#read_all_news').hide();
			}
		});
}

function load_categories(){
		$.ajax({
		type: "POST",
		url: "../../main/list_categories/",
		success: function(data, status){
			var categories = JSON.parse(data);

			var num = categories.length;

			for(var i = 0; i < num; i++){
				$('#list_categories').append("<button id='" + categories[i].cod + "' value='" + categories[i].cod + "' class='btn btn-primary' onclick='search_news(this)'>" + categories[i].name + "</button>");
			}
		}
	});
}

function search_news3(comp){
	var cat = comp.id;
	var sub = comp.value;
	var spec = comp.name;

	var array = {cat: cat, sub: sub, spec: spec};

	    $.ajax({
		type: "POST",
		url: "../../main/search_specific_news/",
		data: {specific_categories: array},
		success: function(data, status){
			var news = JSON.parse(data);
			list_news(news);
			$('#list_specific_categories').html('');
			}
		});
}

function list_first_time(){
	 $.post("../../main/page", {'page': 1}, function(data,status){
                    	json = JSON.parse(data);
                    	list_news(json);
                    	$('.pagination_news').show();
                    });
}

function list_news(news){
	var num = news.length;
	if (num > 0){
		var div_table = document.getElementById("list_news");
		var table = document.createElement("table");
		var tblBody = document.createElement("tbody");
		var tr = document.createElement("tr");
		var th_author = document.createElement("th");
		var th_headline = document.createElement("th");
		var th_event_date = document.createElement("th");
		var table_author = document.createTextNode("Autor");
		var table_headline = document.createTextNode("Titular");
		var table_event_date = document.createTextNode("Fecha");
		tr.appendChild(th_author);
		tr.appendChild(th_headline);
		tr.appendChild(th_event_date);
		th_author.appendChild(table_author);
		th_headline.appendChild(table_headline);
		th_event_date.appendChild(table_event_date);
		tblBody.appendChild(tr);

		for ( var i = 0; i < num; i++){
			var row = document.createElement("tr");
			var column1 = document.createElement("td");
			var textColumn1 = document.createTextNode(news[i].author);
			column1.appendChild(textColumn1);
			var column2 = document.createElement("td");
			var textColumn2 = document.createTextNode(news[i].headline);
			column2.appendChild(textColumn2);
			var column3 = document.createElement("td");
			var textColumn3 = document.createTextNode(news[i].event_date);
			column3.appendChild(textColumn3);
			row.appendChild(column1);
			row.appendChild(column2);
			row.appendChild(column3);
			tblBody.appendChild(row);
		}

		table.appendChild(tblBody);
		div_table.appendChild(table);
		table.setAttribute("border", "2");

		$('#list_categories').html('');
		$('#list_subcategories').html('');
		$('#specific_categories').html('');
		$('#more').hide();
		$('#read_all_news').hide();
		$('#back').show();



	}else{
		reset();
	}
	
}

function bootpag(){
		$.ajax({
		type: "POST",
		url: "../../main/number_pages/",
		success: function(data, status){
		json = JSON.parse(data);
		var number_news = json["num_news"];
		var number_list = 3;
		var pages = Math.ceil(number_news / number_list);

		 if (pages !== 0) {
            refresh();
            $(".pagination_news").bootpag({
                total: pages,
                page: 1,
                maxVisible: 5,
                next: 'Sig',
                prev: 'Ant'
            }).on("page", function (e, num) {
            	reset();
                e.preventDefault();
                    $.post("../../main/page", {'page': num}, function(data,status){
                    	json = JSON.parse(data);
                    	list_news(json);
                    });
            });
        } else {
            $("#results").load("../../main/view_error");
            $('.pagination_news').html('');
            reset();
        }
		}
	});
}

function refresh() {
    $('.pagination_news').html = '';
    $('.pagination_news').val = '';
}

function reset() {
    $('#list_news').html('');
}