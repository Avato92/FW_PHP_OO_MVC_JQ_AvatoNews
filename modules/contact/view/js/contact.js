function paint(dataString) {
    $("#resultMessage").html(dataString).fadeIn("slow");
                    
    setTimeout(function() {
        $("#resultMessage").fadeOut("slow")
    }, 5000);
                            
    $('#contact_form')[0].reset();
                            
    $('.ajaxLoader').fadeOut("fast");
                            
    $('#submitBtn').attr('disabled', false);
}

$(document).ready(function(){
    $(function(){
        $('#submitBtn').attr('disabled', false);
    });
            
	$("#contact_form").validate({
				rules:{
					inputName:{
                        required: true
					},
					inputEmail:{
                        required: true,
                        email: true
					},
                    inputMessage:{
                        required: true
                    }
				},
        highlight: function(element) {
            $(element).closest('.control-group').removeClass('success').addClass('error');},
        success: function(element) {
            $(element).closest('.control-group').removeClass('error').addClass('success');
            $(element).closest('.control-group').find('label').remove();
        },
        errorClass: "help-inline"
	});
	
    $("#contact_form").submit(function(){
        if ($("#contact_form").valid()){
            
            $('#submitBtn').attr('disabled', true);

            $('.ajaxLoader').fadeIn("fast");

            var dataString = $("#contact_form").serialize();
            $.ajax({
                type: "POST",
                url: "../../contact/sent_email/",
                data: dataString,
                success: function(dataString) {
                	console.log(dataString);
                    paint(dataString);
                }
            })
            .fail(function() {
                paint("<div class='alert alert-error'>Error del servidor. Intentelo más tarde</div>");
            });
        }
        return false;
    });
});