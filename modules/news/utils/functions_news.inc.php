<?php

function validate_new($new){
	$error = array();
	$check = true;
	$filter = array(
		'author' => array(
			'filter' => FILTER_VALIDATE_REGEXP,
			'options' => array('regexp' => '/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/')
		),
		'event_date' => array(
			'filter' => FILTER_VALIDATE_REGEXP,
			'options' => array('regexp' =>'/^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/')
		),
		'publication_date' => array(
			'filter' => FILTER_VALIDATE_REGEXP,
			'options' => array('regexp' => '/^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/')
		),
	);

	$result = filter_var_array($new, $filter);

	$result['id_new'] = $new['id_new'];
	$result['headline'] = $new['headline'];
	$result['allied'] = $new['allied'];
	$result['category'] = $new['category'];
	$result['path'] = $new['path'];

	if(count($result['allied']) < 1 ){
		$error['allied'] = "Seleccione mínimo 1 relación";
		$check = false;
	}
	if($result['event_date'] && $result['publication_date']){
        $dates = validate_date($result['event_date'],$result['publication_date']);
        if($dates){
            $error['event_date'] = 'La fecha del suceso debe ser anterior a la de publicación';
            $error['publication_date'] = 'La fecha de publicación debe ser posterior a la del suceso';
            $check = false;
        }
    }
    return $return = array('result' => $check, 'error' => $error, 'data' => $result );
}

function validate_date($date1,$date2){
  $event_date = $date1;
  $publication_date = $date2;

  $event_date = implode('', array_reverse(explode('/', $event_date)));
  $publication_date = implode('', array_reverse(explode('/', $publication_date)));

    if ($publication_date < $event_date){
        return true;
    }
        return false;
}