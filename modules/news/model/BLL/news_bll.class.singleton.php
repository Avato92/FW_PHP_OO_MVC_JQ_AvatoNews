<?php

// $path = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/';
// define('ROOT_PATH', $path);
// define('MODEL_ROOT', ROOT_PATH . 'model/');

// require(MODEL_ROOT . "Db.class.singleton.php");
// require(ROOT_PATH . "modules/news/model/DAO/news_dao.class.singleton.php");

class news_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = news_DAO::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function create_new_BLL($arrArgument){
      return $this->dao->create_new_DAO($this->db, $arrArgument);
    }

    public function obtain_categories_BLL(){
      return $this->dao->obtain_categories_DAO($this->db);
    }

    public function obtain_subcategories_BLL($category){
      return $this->dao->obtain_subcategories_DAO($category, $this->db);
    }

    public function obtain_specific_categories_BLL($arrArgument){
      return $this->dao->obtain_specific_categories_DAO($arrArgument, $this->db);
    }
}
