$(document).ready(function () {
    load_new();
});

function load_new() {
    $.ajax({
        type: 'POST',
        url: "../../news/load/",
    }).done(function (data) {
        var new_json = JSON.parse(data);

        print_new(new_json);

    }).fail(function (xhr) {
        alert(xhr.responseText);
    });
}

function print_new(info) {
    var content = document.getElementById("content");
    var div_product = document.createElement("div");
    var parrafo = document.createElement("p");

    var author = document.createElement("div");
    author.innerHTML = "Autor: ";
    author.innerHTML += info.new.author;

    var id_new = document.createElement("div");
    id_new.innerHTML = "ID: ";
    id_new.innerHTML += info.new.id_new;

    var event_date = document.createElement("div");
    event_date.innerHTML = "Fecha del suceso: ";
    event_date.innerHTML += info.new.event_date;

    var publication_date = document.createElement("div");
    publication_date.innerHTML = "Fecha de publicación: ";
    publication_date.innerHTML += info.new.publication_date;

    var allied = document.createElement("div");
    allied.innerHTML = "Relacionado: ";
    for(var i =0;i < info.new.allied.length;i++){
    allied.innerHTML += " - "+info.new.allied[i];
    }

    var headline = document.createElement("div");
    headline.innerHTML = "Titular: ";
    headline.innerHTML += info.new.headline;

    var category = document.createElement("div");
    category.innerHTML = "Categoria: ";
    category.innerHTML += info.new.category;

    var path = document.createElement("div");
    path.innerHTML = "Ruta de la noticia: ";
    path.innerHTML += info.new.path;

    div_product.appendChild(parrafo);
    parrafo.appendChild(author);
    parrafo.appendChild(id_new);
    parrafo.appendChild(headline);
    parrafo.appendChild(event_date);
    parrafo.appendChild(publication_date);
    parrafo.appendChild(category);
    parrafo.appendChild(allied);
    parrafo.appendChild(path);
    content.appendChild(div_product);
}