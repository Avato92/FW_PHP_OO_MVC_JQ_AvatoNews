Dropzone.autoDiscover = false;

$(document).ready(function () {

    $( "#event_date" ).datepicker({
        dateFormat: 'dd/mm/yy',
        changeMonth: true, changeYear: true,
        minDate: -10, maxDate: 0
    });
    $( "#publication_date" ).datepicker({
      	dateFormat: 'dd/mm/yy',
      	changeMonth: true, changeYear: true,
      	minDate: 0, maxDate: +10
    });

    load_categories_v1();


    $("#category1").change(function() {
    var category = $(this).val();

    load_subcategories_v1(category);
  });

    $("#subcategory").change(function() {
    var subcategory = $(this).val();

    load_specific_categories_v1(subcategory);
  });


    $('#submit_new').click(function(){
        var author = document.getElementById('author').value;
        var id = document.getElementById('id_new').value;
        var headline = document.getElementById('headline').value;
        var event_date = document.getElementById('event_date').value;
        var publication_date = document.getElementById('publication_date').value;
        var allied = [];
        var allied_checked = document.getElementsByClassName('allied');
        var j = 0;
        for (var i=0; i< allied_checked.length; i++){
          if (allied_checked[i].checked){
            allied[j] = allied_checked[i].value;
            j++;
          }
        }
        var category = document.getElementById('category').value;
        var path = document.getElementById('path_new').value;
        var check = validate_news();
        if(check){
          var data_new = {"author": author, "id_new": id, "headline": headline, "event_date": event_date, "publication_date": publication_date, "allied": allied, "category": category, "path": path};
          var data_new_JSON = JSON.stringify(data_new);

          $.post('../../news/upload_new/',
            {upload_new:data_new_JSON},
            function(response){
              var data = JSON.parse(response);

              if(data[0] == true){
                window.location.href=data[1];
              }
            });
      }
    });

        $("#dropzone").dropzone({
        url: "../../news/upload/",
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "Ocurrió un error con el servidor",
        acceptedFiles: 'image/*,.txt,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Archivo subido con éxito').addClass('msg_ok').animate({'right': '300px'}, 300);
            });
        },
        complete: function (file) {
            if(file.status == "success"){
            alert("El archivo se ha subido correctamente: " + file.name);
            }
        },
        error: function (file) {
            alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            $.ajax({
                type: "POST",
                url: "../../news/delete",
                data: "filename=" + name,
                success: function (data) {
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(data);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) !== null) {
                            element.parentNode.removeChild(file.previewElement);
                        } else {
                            return false;
                        }
                    } else {
                        var element2;
                        if ((element2 = file.previewElement) !== null) {
                            element2.parentNode.removeChild(file.previewElement);
                        } else {
                            return false;
                        }
                    }

                }
            });
        }
    });
});

function validate_news(){
  var error = true;
  var allied = [];
  var allied_checked = document.getElementsByClassName('allied');
  var j = 0;
  for (var i=0; i< allied_checked.length; i++){
        if (allied_checked[i].checked){
          allied[j] = allied_checked[i].value;
          j++;
        }
    }
  var category = document.getElementById('category').value;

  var author_reg = /^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/;
  var date_reg =/^([0][1-9]|[12][0-9]|3[01])(\/|-)([0][1-9]|[1][0-2])\2(\d{4})$/;

  $(".error").remove();
  if ($("#author").val() === "" ){
      $("#author").focus().after("<span class='error'>Introduzca el nombre del autor</span>");
      error = false;
  }else if(!author_reg.test($("#author").val())){
      $("#author").focus().after("<span class='error'>Nombre introducido incorrectamente</span>");
      error = false;
    }
  if ($("#id_new").val() === ""){
    $("#id_new").focus().after("<span class='error'>Introduzca el ID de la noticia</span>");
    error = false;
    }
  if ($('#headline').val() === ""){
    $("#headline").focus().after("<span class='error'>Introduzca el título</span>");
    error = false;
    }
  if  ($('#event_date').val()=== ""){
    $('#event_date').focus().after("<span class='error'>Introduzca fecha</span>");
    error = false;
  }else if(!date_reg.test($("#event_date").val())){
    $("#event_date").focus().after("<span class='error'>Formato fecha incorrecto</span>");
    error = false;
    }
  if ($('#publication_date').val()=== ""){
    $('#publication_date').focus().after("<span class='error'>Introduzca fecha</span>");
    error = false;
  }
  if (allied.length <=  0){
    $("#error_allied").after("<span class='error'>Seleccione mínimo uno</span>");
    error = false;
    }
  if (category === ""){
    $("#error_category").after("<span class='error'>Seleccione la categoria</span>");
    error = false;
    }
  if ($('#path_new').val()=== ""){
    $('#path_new').focus().after("<span class='error'>Escriba la ruta de la noticia</span>");
    error = false;
  }
  return error;
}

function load_categories_v1() {
    $.get("../../news/load_category",
        function( response ) {

            if(response === 'error'){
                load_categories_v2("resources/listofcategories.json");
            }else{
                load_categories_v2("../../news/load_category/");
            }
    })
    .fail(function(response) {
        load_categories_v2("resources/listofcategories.json");
    });
}

function load_categories_v2(cad) {
    $.getJSON( cad, function(data) {
      $("#category1").empty();
      $("#category1").append('<option value="" selected="selected">Seleccione una categoria</option>');

      $.each(data, function (i, valor) {
        $("#category1").append("<option value='" + valor.cod + "'>" + valor.name + "</option>");
      });
    $("#subcategory").empty();
    $("#subcategory").append('<option value="" selected="selected">Seleccione una subcategoria</option>');
    $("#subcategory").prop('disabled', true);
    $("#specific_category").empty();
    $("#specific_category").append('<option value="" selected="selected">Seleccione una categoria especifica</option>');
    $("#specific_category").prop('disabled', true);
    })
    .fail(function() {
        alert( "error load_categories" );
    });
}

function load_subcategories_v1(category) {
    $.getJSON("../../news/load_subcategories&cat=" + category , function(data) {
      $("#subcategory").empty();
      $("#subcategory").append('<option value="" selected="selected">Seleccione una subcategoria</option>');
      $.each(data, function (i, valor) {
        $("#subcategory").append("<option value='" + valor.cod + "'>" + valor.name + "</option>");
      });
    $("#subcategory").prop('disabled', false);
    $("#specific_category").empty();
    $("#specific_category").append('<option value="" selected="selected">Seleccione una categoria especifica</option>');
    $("#specific_category").prop('disabled', true);
    })
    .fail(function() {
        load_subcategories_v2(category);
    });
}

function load_subcategories_v2(category) {
    $.get("resources/subcategories_and_specific_categories.xml", function (xml) {
      $("#subcategory").empty();
      $("#subcategory").append('<option value="" selected="selected">Selecciona una subcategoria</option>');

        $(xml).find("subcategory[id=" + category + ']').each(function () {
            var id = $(this).attr('id');
            var name = $(this).find('nombre').text();
            $("#subcategory").append("<option value='" + id + "'>" + name + "</option>");
        });
    })
    .fail(function() {
        alert( "error load_subcategories" );
    });
}

function load_specific_categories_v1(subcategory) {
    $.getJSON("../../news/load_specific_categories&subcat=" + subcategory, function(data) {
      $("#specific_category").empty();
      $("#specific_category").append('<option value="" selected="selected">Seleccione una categoria especifica</option>');
      $.each(data, function (i, valor) {
        $("#specific_category").append("<option value='" + valor.cod + "'>" + valor.concrete_name + "</option>");
      });
    $("#specific_category").prop('disabled', false);
    })
    .fail(function() {
      load_specific_categories_v2(subcategory);
    });
}

function load_specific_categories_v2(subcategory) {
    $.get("resources/subcategories_and_specific_categories.xml", function (xml) {
      var category = $("#category1");
      $("#specific_category").empty();
      $("#specific_category").append('<option value="" selected="selected">Selecciona una categoria especifica</option>');

        $(xml).find("category[id="+ category + "]/subcategory[id=" + subcategory + "]").each(function () {
            var id = $(this).attr('id');
            var name = $(this).find('nombre').text();
            $("#specific_category").append("<option value='" + id + "'>" + name + "</option>");
        });
    })
    .fail(function() {
        alert( "error load_specific_categories" );
    });
}