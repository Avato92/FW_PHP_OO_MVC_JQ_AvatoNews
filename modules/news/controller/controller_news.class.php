<?php

class controller_news {
    function __construct() {
        require_once(UTILS_PATH_NEWS . "functions_news.inc.php");
        $_SESSION['module'] = "news";
    }

    function begin() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/news/view/', 'create_news.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function upload_new(){
      upload_new2();
    }

    function load(){
      $data_new = array();

      if (isset($_SESSION['new'])) {
        $data_new["new"] = $_SESSION['new'];
      }
      echo json_encode($data_new);
      exit();
    }

    function upload(){
      $result_file = upload_files();
      $_SESSION['result_file'] = $result_file;
      echo json_encode($result_file);
    }

    function delete(){
      $_SESSION['result_file'] = array();
      $result = remove_files();
      if($result === true){
        echo json_encode(array("res" => true));
      }else{
        echo json_encode(array("res" => false));
      }
    }

    function load_category(){
      $json = array();

      $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/news/model/model/';
      $json = loadModel($path_model, "news_model", "obtain_categories");
    
      if($json){
        echo json_encode($json);
        exit;
      }else{
        $json = "error";
        echo json_encode($json);
        exit;
      }
    }

    function load_subcategories(){
      $json = array();
      $_SESSION['cat'] = $_GET['cat'];
      $category = $_GET['cat'];
      $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/news/model/model/';
      $json = loadModel($path_model, "news_model", "obtain_subcategories", $category);

      if($json){
        echo json_encode($json);
        exit;
      }else{
        $json = "error";
        echo json_encode($json);
        exit;
      }
    }

    function load_specific_categories(){
      $json = array();
      $category = $_SESSION['cat'];
      $subcategory = $_GET['subcat'];
      $find_cat = array(
        'cat' => $category,
        'subcat' => $subcategory);

      $path_model=$_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/news/model/model/';
      $json = loadModel($path_model, "news_model", "obtain_specific_categories", $find_cat);

      if($json){
        echo json_encode($json);
        exit;
      }else{
        $json = "error";
        echo json_encode($json);
        exit;
      }
    }
        function result_new() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/news/view/', 'result_new.html');

        require_once(VIEW_PATH_INC . "footer.html");
    }
}

  function upload_new2(){
  $rdo = array();
  $check = array();
  $newJSON = json_decode($_POST['upload_new'], true);
  $confirm = validate_new($newJSON);

  if($confirm['result']){
    $new_argument = array(
      'author' => $confirm['data']['author'],
      'id_new' => $confirm['data']['id_new'],
      'headline' => $confirm['data']['headline'],
      'event_date' => $confirm['data']['event_date'],
      'publication_date' => $confirm['data']['publication_date'],
      'allied' => $confirm['data']['allied'],
      'category' => $confirm['data']['category'],
      'path' => $confirm['data']['path']
    );
      $check_array = false;
      $path_model = $_SERVER['DOCUMENT_ROOT'] . '/FW_PHP_OO_JQ_AvatoNews/modules/news/model/model/';
      $check_array = loadModel($path_model, "news_model", "create_new", $new_argument);

        if ($check_array){
          $message = "Noticia creada con éxito";
          $_SESSION['new'] = $new_argument;

           $callback="../../news/result_new/";

          $check['success'] = true;
          $check['redirect'] = $callback;

          foreach($check as $row){
            array_push($rdo, $row);
          }

          echo json_encode($rdo, JSON_FORCE_OBJECT);
          exit();
        }else{
          $message = "No se ha podido registrar la noticia";
          echo json_encode($message);
          exit();
        }  
  }else{
      $check['success'] = false;
      $check['error'] = $confirm['error'];

      foreach($check as $row){
          array_push($rdo, $row);
        }
    }
    header('HTTP/1.0 400 Bad error');
    echo json_encode($rdo, JSON_FORCE_OBJECT);
    exit();
  }
